const getResultsOfGame = (rooms, currentRoomName) => {
  const roomInfo = rooms.get(currentRoomName)
  if (roomInfo) {
    const resultsOfFinishedUsers = roomInfo.usersList
      .filter(user => user.hasOwnProperty('endTime'))
      .sort((a, b) => a.endTime - b.endTime)
      .map(user => { return { name: user.name, endInfo: ` он потратил ${(user.endTime - user.startTime) / 1000} секунд на гонку` } })

    const resultsOfUnfinishedUsers = roomInfo.usersList
      .filter(user => !user.hasOwnProperty('endTime'))
      .sort((a, b) => b.progress - a.progress)
      .map(user => { return { name: user.name, endInfo: `он не закончил гонку, но его прогресс равен ${Math.round(user.progress)}%` } })

    const arrayWithWinners = resultsOfFinishedUsers.concat(resultsOfUnfinishedUsers).slice(0, 3)
    let resultString = ''
    for (let i = 0; i < arrayWithWinners.length; i++) {
      resultString = resultString.concat(`${i + 1}-ое место занял ${arrayWithWinners[i].name}, ${arrayWithWinners[i].endInfo}\n`)
    }
    return resultString
  }
}

const checkForEndGame = (rooms, currentRoomName) => {
  const roomInfo = rooms.get(currentRoomName)
  const isEveryUserEnd = roomInfo.usersList.every(item => item.progress == 100 ? true : false)
  if (isEveryUserEnd) {
    roomInfo.gameActive = false
    return true
  }
  return false
}

const checkIsAllUsersReady = (roomInfo) => {
  const isReadyAll = roomInfo.usersList.every(item => {
    return item.isReady
  })
  return (isReadyAll)
}

export {
  checkForEndGame, getResultsOfGame, checkIsAllUsersReady
}