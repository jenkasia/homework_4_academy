import * as config from "./config";
import { formatString } from "./helpers/formatData"
import { isUserNameExist } from "./helpers/userHelper"
import { checkForEndGame, getResultsOfGame, checkIsAllUsersReady } from "./helpers/roomHelper"
import MessageFactory from './bot_commentator'
const rooms = new Map()
const users = new Map()

export default io => {

  io.on("connection", socket => {
    let messageFactory
    let currentRoomName = ''
    const userName = formatString(socket.handshake.query.username);
    if (isUserNameExist(userName, users,)) {
      socket.emit('REDIRECT_TO_LOGIN', userName)
      return
    }
    else {
      socket.emit('RENDER_ROOMS', Object.fromEntries(rooms))
      users.set(userName, { socketId: socket.id, gamesWasPlayed: 0 })
    }

    const isRoomExist = (roomName) => {
      if (rooms.has(roomName)) {
        return true
      }
      else {
        return false
      }
    }

    const removeUserFromRoom = (roomName, socketId) => {
      const roomInfo = rooms.get(roomName)
      roomInfo.usersList = roomInfo.usersList.filter(item => item.socketId !== socketId)
    }

    const joinRoom = (roomName) => {
      const roomInfo = rooms.get(roomName)
      messageFactory = new MessageFactory(roomInfo)
      if (!roomInfo) return
      roomInfo.usersInRoom++
      if (roomInfo.usersInRoom === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        roomInfo.isFull = true
      }
      roomInfo.usersList.push({
        socketId: socket.id,
        name: userName,
        isReady: false,
        progress: 0,
        gamesWasPlayed: users.get(userName).gamesWasPlayed
      })
      socket.join(roomName, () => {
      });
      socket.emit("JOIN_ROOM_DONE", rooms.get(roomName))
      socket.broadcast
        .to(roomName)
        .emit(
          'JOIN_ROOM_DONE',
          rooms.get(roomName)
        );
      const allRooms = Object.fromEntries(rooms)
      io.emit('RENDER_ROOMS', allRooms)
    }

    const leaveRoom = (roomName) => {
      const roomInfo = rooms.get(roomName)
      if (!roomInfo) {
        return
      }
      roomInfo.usersInRoom -= 1
      socket.emit('LEAVE_ROOM_DONE', { roomName, roomInfo })
      if (roomInfo.usersInRoom < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        roomInfo.isFull = false
      }
      socket.leave(roomName, () => {
        io.to(roomName).emit("UPDATE_PROGRESS_BARS", roomInfo)
      });
      removeUserFromRoom(roomName, socket.id)
      if (roomInfo.usersInRoom <= 0) {
        rooms.delete(roomName)
      }
      else {
        io.to(roomName).emit("UPDATE_PROGRESS_BARS", rooms.get(roomName))
      }

      const isAllUsersReady = checkIsAllUsersReady(roomInfo)
      if (isAllUsersReady && !roomInfo.gameActive) {
        roomInfo.isReadyToStart = true
        startGame(config.SECONDS_TIMER_BEFORE_START_GAME, roomInfo)
      }
      const allRooms = Object.fromEntries(rooms)
      io.emit('RENDER_ROOMS', allRooms)
    }

    const resetGame = (roomInfo) => {
      roomInfo.isReadyToStart = false;
      roomInfo.gameActive = false;
      roomInfo.usersList.forEach(user => {
        user.progress = 0
        delete user.endTime
        user.isReady = false
        user.gamesWasPlayed += 1
        users.get(user.name).gamesWasPlayed += 1
      });
      io.to(roomInfo.roomName).emit('RESET_ROOM', roomInfo)
      io.emit('RENDER_ROOMS', Object.fromEntries(rooms))
    }

    const gameTimer = (roomName) => {
      const roomInfo = rooms.get(roomName)
      if (!roomInfo) {
        return
      }
      let timeLeft = config.SECONDS_FOR_GAME
      io.to(roomName).emit('UPDATE_GAME_TIMER', timeLeft)
      const interval = setInterval(() => {
        roomInfo.timeLeft = timeLeft - 1
        if (!roomInfo.gameActive) {
          clearInterval(interval);
          return
        }
        io.to(roomName).emit('UPDATE_GAME_TIMER', timeLeft - 1)
        timeLeft--;
        if (timeLeft <= 0) {
          clearInterval(interval);
          if (roomInfo && roomInfo.gameActive) {
            io.to(roomInfo.roomName).emit('RENDER_BOT_MESSAGE', messageFactory.create('endGameMessage'))
            io.to(roomName).emit('GAME_END')
            resetGame(roomInfo)
          }
        }
      }, 1000);
    }

    const getRandomTimeToGenerateJoke = () => {
      const minIntervalTime = 10000
      const maxIntervalTime = 20000
      let randomInterval = minIntervalTime + Math.random() * (maxIntervalTime + 1 - minIntervalTime);
      return Math.floor(randomInterval);
    }

    const generateRandomFactOrJoke = (roomInfo) => {
      if (roomInfo && roomInfo.gameActive) {
        setTimeout(() => {
          if (!roomInfo || !roomInfo.gameActive) return
          io.to(roomInfo.roomName).emit('RENDER_BOT_MESSAGE', messageFactory.create('randomFactOrJoke'))
          generateRandomFactOrJoke(roomInfo)
        }, getRandomTimeToGenerateJoke());
      } else {
        return
      }
    }

    const generateStateOfRace = (roomInfo) => {
      if (roomInfo && roomInfo.gameActive) {
        setTimeout(() => {
          if (!roomInfo || !roomInfo.gameActive) return
          io.to(roomInfo.roomName).emit('RENDER_BOT_MESSAGE', messageFactory.create('gameStatusMessage'))
          generateStateOfRace(roomInfo)
        }, 30000);
      } else {
        return
      }
    }

    const createStartTimeForUsers = (roomInfo) => {
      const startTime = Date.now()
      roomInfo.usersList.forEach(user => user.startTime = startTime)
    }

    const startGame = (seconds, roomInfo) => {
      io.to(roomInfo.roomName).emit('RENDER_BOT_MESSAGE', messageFactory.create('startMessage'))
      io.to(roomInfo.roomName).emit('RENDER_BOT_MESSAGE', messageFactory.create('racerListMessage'))
      let textNumber = Math.floor(Math.random() * Math.floor(config.TEXT_COUNT));
      roomInfo.gameActive = true
      roomInfo.secondsBeforeStart = seconds - 1
      io.to(currentRoomName).emit('START_TIMER_BEFORE_GAME', seconds)
      io.to(currentRoomName).emit('GET_TEXT_TO_GAME', textNumber)
      const interval = setInterval(() => {
        io.to(roomInfo.roomName).emit('START_TIMER_BEFORE_GAME', roomInfo.secondsBeforeStart)
        roomInfo.secondsBeforeStart--;
        if (roomInfo.secondsBeforeStart < 0) {
          clearInterval(interval);

          generateRandomFactOrJoke(roomInfo)
          generateStateOfRace(roomInfo)
          io.to(roomInfo.roomName).emit('START_GAME')
          createStartTimeForUsers(roomInfo)
          gameTimer(roomInfo.roomName)
        }
      }, 1000);
    }

    socket.on('UPDATE_PROGRESS', (progress) => {
      const roomInfo = rooms.get(currentRoomName)
      let user = roomInfo.usersList.find(el => el.socketId == socket.id)
      user.progress = progress
      io.to(currentRoomName).emit('UPDATE_PROGRESS_BARS', roomInfo)

      if (progress == 100) {
        user.endTime = Date.now()
        io.to(roomInfo.roomName).emit('RENDER_BOT_MESSAGE', messageFactory.create('finishMessage'))
        if (checkForEndGame(rooms, currentRoomName)) {
          const gameResults = getResultsOfGame(rooms, currentRoomName)
          roomInfo.usersList.forEach
          io.to(roomInfo.roomName).emit('RENDER_BOT_MESSAGE', messageFactory.create('endGameMessage'))
          resetGame(roomInfo)
        }
      }
    })

    socket.on('JOIN_ROOM', (roomName) => {
      joinRoom(roomName)
      currentRoomName = roomName
    })

    socket.on('LEAVE_ROOM', (roomName) => {
      leaveRoom(roomName)
      currentRoomName = ''
    })

    socket.on('CREATE_ROOM', (roomName) => {
      roomName = formatString(roomName)
      if (isRoomExist(roomName)) {
        socket.emit('ERROR_ROOM_EXIST', `Room with name ${roomName}, already exist`)
        return
      }
      else {
        const usersList = []
        const roomInfo = {
          roomName: roomName,
          usersInRoom: 0,
          isFull: false,
          isReadyToStart: false,
          secondsBeforeStart: 0,
          usersList: usersList,
          gameActive: false
        }
        rooms.set(roomName, roomInfo)
        joinRoom(roomName)
        currentRoomName = roomName
      }
    })

    socket.on('CHANGE_USER_STATUS', () => {
      const roomInfo = rooms.get(currentRoomName)
      let user = roomInfo.usersList.filter(user => user.socketId === socket.id)[0]
      user.isReady = !user.isReady
      let isAllUsersReady = checkIsAllUsersReady(roomInfo)
      io.to(currentRoomName).emit("JOIN_ROOM_DONE", rooms.get(currentRoomName))
      if (isAllUsersReady) {
        roomInfo.isReadyToStart = true
        startGame(config.SECONDS_TIMER_BEFORE_START_GAME, roomInfo)
      }
      else {
        roomInfo.isReadyToStart = false
      }
      io.emit('RENDER_ROOMS', Object.fromEntries(rooms))
    })

    socket.on('NOTIFY_USERS_WHEN_ONE_OF_RACER_NEAR_FINISH', () => {
      const userNearFinish = rooms.get(currentRoomName).usersList.find(user => user.socketId === socket.id)
      io.to(currentRoomName).emit('RENDER_BOT_MESSAGE', messageFactory.create('beforeFinishMessage', userNearFinish))
    })


    socket.on('disconnect', () => {
      leaveRoom(currentRoomName)
      io.emit('RENDER_ROOMS', Object.fromEntries(rooms))
      users.delete(userName)
    })

  });
};


