const messagesTemplates = {
  startMessage: [
    (botName, gamePlace) => `На улице сейчас немного пасмурно, но на ${gamePlace} сейчас просто замечательная атмосфера: двигатели рычат, зрители улыбаются а гонщики едва заметно нервничают и готовят своих железных коней к заезду. А комментировать всё это действо буду я, ${botName} и я рад вас приветствовать со словами Доброго Вам дня, господа!`,

    (botName, gamePlace) => `Добрый день, сегодня вами Я ${botName}, лучший комментатор TYPI-гонок и сейчас мы будм наблюдать как на ${gamePlace} будут сражаться за победу Клавиатурные гонщики. Ожидаем готовности и присступаем`,

    (botName, gamePlace) => `Наконецто, наступил догоджанный день, где мы с Вами можем наблюдать за ходом исторической битвы за Клавиатурный кубок. Сегодняшний поединок будет полон эмоций для каждого, кто находится на ${gamePlace} а так же для меня, коментатора сегодняшнего поединеп ${botName} Гонщики готовятся к поединку а мы готовы приступать`,
  ],

  racerListMessage: [
    `А тем временем давайте посмотрим на список гонщиков`,
    `И давайте узнаем кто сегодня сражается за первенство в нашем поединке`,
    `А тем вреемеенем, пока гонщики готовятся к поединку, дайте узнаем их имена`
  ],

  randomFactOrJoke: [
    '56% печатного текста набирается левой рукой',
    ' Буква латиницы С находится на той же кнопке, где и русская С, в то время как кнопка А/Ф обратная клавише F/A.',
    'А вы знали что клавиатура в 20 тысяч раз грязнее унитаза',
    'Кстати, самое длинное слово, которое можно напечатать правой рукой - lollipop,',
    'Самой распространенной, а потому и часто нажимаемой на. "клаве" гласной в английском языке по праву признана E, а согласной - T .',
    'Интресный факт: Капля никотина убивает лошадь. А кружка кофе—клавиатуру',
    `Анекдот\n— Вы акула пера?\n— Нет, дятел клавиатуры!`,
    `Хочешь печеньку?
    1) Переверни клавиатуру
    2) Потряси как следует
    3) Соедини все крошки в единый комочек
    4) Поставь запекаться на 10 минут в духовку`
  ],

  beforeFinishMessage: [
    (name) => `Еще немножко и гонщик ${name} будет на финише`,
    (name) => `Воу-воу ${name} уже на финишной прямой`,
    (name) => `${name} приближается к финишу, пора бы всем ускориться`,
    (name) => `Гонка вроде только началась, а ${name} почи на финише`,
    (name) => `Зрители не успели могнуть, как ${name} подбираеться к черте финиша`,
  ],

  finishMessage: [
    (name) => `Участик с именем ${name} успешно завершает гонку, давайте его поздравим`,
    (name) => `Посмотрите, это же ${name} и он уже на финише`,
    (name) => `${name} уже на финише, давайте его поздравим 😎`,
    (name) => `${name} пересек финишную прямую, теперь можно выдохнуть`,
    (name) => `Как же так, гонка только-только наачалась, а ${name} уже на финише`,
    (name) => `Несмотря на все сложности, гонка для ${name} позади, теперь на его счету на 1 гонку больше`,
  ],
}

const carList = ['Audi', 'Mersedess', 'BMW', 'Porshe', "Ferari", 'Lamborghini', 'Nissan', 'Toyota', 'Bently', 'Subary']

//____________________________________________________________
//___________________FACTORY_PATTERTN_START___________________
//____________________________________________________________

class StartMessage {
  constructor(textReturnFunction) {
    this.botName = 'КоментBотер 🤖'
    this.gamePlace = '"Трассе локалхост 3002"'
    this.text = textReturnFunction(this.botName, this.gamePlace)
  }
}

class RacerListMessage {
  constructor(text, room) {
    let usersList = room ? this.generateUserList(room.usersList) : ''
    this.text = text.concat(usersList)
  }
  generateUserList(users) {
    let cars = users.map(user => {
      user.car = this.generateRandomCar()
      return `\n${user.name} всего сыграл ${user.gamesWasPlayed} игр, а сегодня выполняет заезд на ${user.car} `
    })
    return cars
  }
  generateRandomCar() {
    return carList[Math.floor(Math.random() * carList.length)]
  }
}

class RandomFactOrJoke {
  constructor(text) {
    this.text = text
  }
}


class GameStatusMessage {
  constructor(text, room) {
    this.text = this.generateTextForMessage(room)
  }
  generateTextForMessage(room) {
    const toEndGameString = `До окончания гонки осталось ${room.timeLeft} секунд`

    let place = 0
    const stringWithPositionsRacersv = room.usersList
      .filter(user => !user.hasOwnProperty('endTime'))
      .sort((a, b) => b.progress - a.progress)
      .map(user => {
        place++
        return `${user.name} прошел гонку на ${Math.round(user.progress)}% и находится на ${place} месте из теех кто не завершил гонку`
      })
      .join('\n')
    return toEndGameString.concat('\n', stringWithPositionsRacersv)
  }
}
class BeforeFinishMessage {
  constructor(text, users, userThatNearFinish) {
    this.text = text(userThatNearFinish.name)
  }
}

class LeaveGameMessage {
  constructor(name) {
    this.name = name
  }

}

class FinishMessage {
  constructor(text, room) {
    const lastFinishedUser = this.getLastFinisherUser(room.usersList)
    this.text = text(lastFinishedUser)
  }
  getLastFinisherUser(users) {
    let finishedUser = users.sort((a, b) => {
      return (b.endTime || 0) - (a.endTime || 0)
    })
    return finishedUser[0].name
  }
}

class EndGameMessage {
  constructor(text, room) {
    this.text = this.getStringWithWinners(room.usersList)
  }
  getStringWithWinners(users) {
    if (users) {
      const resultsOfFinishedUsers = users
        .filter(user => user.hasOwnProperty('endTime'))
        .sort((a, b) => a.endTime - b.endTime)
        .map(user => { return { name: user.name, endInfo: ` он потратил ${(user.endTime - user.startTime) / 1000} секунд на гонку` } })

      const resultsOfUnfinishedUsers = users
        .filter(user => !user.hasOwnProperty('endTime'))
        .sort((a, b) => b.progress - a.progress)
        .map(user => { return { name: user.name, endInfo: `он не закончил гонку, но его прогресс равен ${Math.round(user.progress)}%` } })

      const arrayWithWinners = resultsOfFinishedUsers.concat(resultsOfUnfinishedUsers).slice(0, 3)
      let resultString = ''
      for (let i = 0; i < arrayWithWinners.length; i++) {
        resultString = resultString.concat(`${i + 1}-ое место занял ${arrayWithWinners[i].name}, ${arrayWithWinners[i].endInfo}\n`)
      }
      return resultString
    }
  }
}


export default class MessageFactory {

  constructor(room) {
    this.room = room
  }

  static list = {
    startMessage: StartMessage,
    racerListMessage: RacerListMessage,
    gameStatusMessage: GameStatusMessage,
    beforeFinishMessage: BeforeFinishMessage,
    leaveGameMessage: LeaveGameMessage,
    finishMessage: FinishMessage,
    endGameMessage: EndGameMessage,
    randomFactOrJoke: RandomFactOrJoke
  }

  create(messageType = 'startMessage', aditionalInfo) {
    let textReturner = this.getRandomTextFromArray(messagesTemplates[messageType])
    const Membership = MessageFactory.list[messageType]
    const message = new Membership(textReturner, this.room, aditionalInfo)
    message.type = messageType
    return message
  }

  getRandomTextFromArray(availableTextsArray) {
    if (availableTextsArray) {
      let amountOfTexts = availableTextsArray.length
      let rand = Math.floor(Math.random() * Math.floor(amountOfTexts));
      return availableTextsArray[rand]
    }
    else return ''
  }
}
//_______________________________________________________
//________________FACTORY_PATTERTN_END___________________
//_______________________________________________________
